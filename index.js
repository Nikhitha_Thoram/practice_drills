const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interests: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

//1.

const result = Object.keys(users).reduce((acc,curr)=>{
    if(users[curr].interests[0].includes('Video Games')){
        acc[curr] =users[curr];
    }
    return acc;
},{});
console.log(result);

2.

const usersInGermany =Object.keys(users).filter(item=>users[item].nationality==='Germany')
console.log(usersInGermany);

const usersInGermanyResult= Object.keys(users).reduce((acc,curr)=>{
    if(users[curr].nationality==='Germany'){
        acc[curr] = users[curr];
    }
    return acc;
},{});

console.log(usersInGermany);

//4.

const mastersDegree=Object.keys(users).filter(item=>users[item].qualification==='Masters')
console.log(mastersDegree);

const mastersDegereeResult=Object.keys(users).reduce((acc,curr)=>{
    if(users[curr].qualification==='Masters'){
        acc[curr] =users[curr];
    }
    return acc;
},{});

console.log(mastersDegeree);

//3.

function sortUsers(data){
    data=Object.fromEntries(Object.entries(data).map(user=>{
    
    if(user[1].desgination.includes('Senior')&& user[1].desgination.includes('Developer')){
     user[1].position =1;
    }
    else if(user[1].desgination.includes('Developer')){
        user[1].position =2;
    }
    else if(user[1].desgination.includes('Intern')){
         user[1].position =3;
    }
    return user;
    }))

    return Object.fromEntries(Object.entries(data).sort(([,user1],[,user2])=>{

     if(user1.position>user2.position){
        return 1;
     }
     else if(user1.position<user2.position){
        return -1;
     }
     else{
        if(user1.age<user2.age){
            return 1;
        }
        else if(user1.age>user2.age){
            return -1
        }
        else{
            return 0;
        }
     }
    }))
}

console.log(sortUsers(users));


//5.

const groupUsers = Object.entries(users).reduce((acc,[name,user])=>{
    let language='';
    if(user.desgination.toLowerCase().includes('golang')){
        language ='golang';
    }
    else if(user.desgination.toLowerCase().includes('javascript')){
        language ='javascript';
    }
    else if(user.desgination.toLowerCase().includes('python')){
        language='python';
    }
    if(!acc[language]){
        acc[language]=[];
    }
    acc[language].push(name);
    return acc;
},{});

console.log(groupUsers);


