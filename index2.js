const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K,Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B,Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.


*/ 




//1.

const available = items.filter(item=>item.available);
console.log(available);

//2.

const vitaminC = items.filter(item=>item.contains ==='Vitamin C');
console.log(vitaminC);

//3.

const vitaminA = items.filter(item=>item.contains.includes('Vitamin A'));
console.log(vitaminA);

//4.

const groupVitamins = items.reduce((acc,curr)=>{
    const vitamins = curr.contains.split(',');
    vitamins.map(vitamin=>{
        if(!acc[vitamin]){
            acc[vitamin]=[];
        }
        acc[vitamin].push(curr.name);
    })
    return acc;
},{});

console.log(groupVitamins);

//5.

const sortVitamins = items.sort((a,b)=>{
    const vitaminA = a.contains.split(',').length;
    const vitaminB = b.contains.split(',').length;
    return vitaminA-vitaminB;
})

console.log(sortVitamins);


